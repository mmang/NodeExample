## Essai de code avec nodejs et mongodb

### Installation

#### NodeJS et npm

    >npm init -y
    >npm install mongodb -S

#### MongoDB community server
	
Sur PC
	>cd C:\
	>mkdir data
	>cd data
	>mkdir db
	>cd "C:\Program Files\MongoDB\Server\3.6\bin"
	>mongod

Sur Mac
    >mongod --config /usr/local/etc/mongod.conf

Le client

Sur PC
	>cd "C:\Program Files\MongoDB\Server\3.6\bin"
	>mongo --host 127.0.0.1:27017

Sur Mac
    >mongo --host 127.0.0.1:27017

    >show dbs

    >use blog
    >db.articles.insert({titre:'Mon premier article',contenu:'bla bla bla'});
    >db.articles.find();

Commandes utiles :

    >db.dropDatabase()
    >db.articles.remove( { } );
    >db.articles.findOne({titre:'article auto'});
    >db.articles.remove({titre:'article auto'});


### Utilisation

    >node example.js

Puis ouvrir http://localhost:3000/ dans votre navigateur préféré