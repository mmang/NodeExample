const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;
const database = 'blog';
const dburl = 'mongodb://localhost/'+database;

// https://zestedesavoir.com/tutoriels/312/debuter-avec-mongodb-pour-node-js/
// https://www.packtpub.com/books/content/writing-blog-application-nodejs-and-angularjs
var MongoClient = require("mongodb").MongoClient

const server = http.createServer((req, res) => {
  console.log('HTTP Request from ',req.connection.remoteAddress);
  console.log(req.headers);
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
    
  MongoClient.connect(dburl, function(error, dbconnection) {
    if (error) throw error;
	console.log('connecté à la base '+database+ "\n");
	res.write('connecté à la base '+database+ "\n");
	
  	const BlogDB = dbconnection.db(database);
	var now = new Date().toLocaleString();
  	var nouvelArticle = { titre: "article auto", contenu: "date: " + now }; 
  //	BlogDB.collection('articles').save(nouvelArticle, { w: 1 }); // Ce document sera inséré
  	
// ne marche pas mais en console marche
 // 	var auto = BlogDB.collection("articles").findOne({'titre':'article auto'});
 // 	console.log('un auto:'+ auto._id);
  	
  	BlogDB.collection("articles").update(
    { titre: "article auto"}, 
    { $set: { contenu: "date: " + now } }
);
 
    var results = BlogDB.collection('articles').find();
    results.each(function(error, article) {
        if (error) throw error;
		if (article != null) {
        console.log('id:'+article._id);
            res.write(
               "Titre : " + article.titre + "\n"           
                + "Contenu : " + article.contenu +  "\n\n"               
            );
		}
		else {
			dbconnection.close();
    		res.end('Hello World\n');
		}
    });
  });
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});